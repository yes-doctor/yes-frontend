import { mount } from '@vue/test-utils'
import Logo from '@/components/Wallet.vue'

describe('Wallet', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Logo)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
