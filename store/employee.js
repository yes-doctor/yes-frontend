export const state = () => ({
  account: null,
  profile: null,
  claims: []
})

export const getters = {
  getAccount: (state) => state.account,
  getProfile: (state) => state.profile,
  getClaims: (state) => state.claims
}

export const mutations = {
  addAccount(state, payload) {
    state.account = payload
  },
  removeAccount(state) {
    state.account = null
    state.profile = null
    state.claims = []
  },
  updateProfile(state, payload) {
    state.profile = payload
  },
  addClaims(state, payload) {
    state.claims.push(payload)
  }
}

export const actions = {
  async addAccount(store, payload) {
    store.commit('addAccount', payload)
  },
  async removeAccount(store) {
    store.commit('removeAccount')
  },
  async updateProfile(store, payload) {
    store.commit('updateProfile', payload)
  },
  async addClaims(store, payload) {
    store.commit('addClaims', payload)
  }
}
