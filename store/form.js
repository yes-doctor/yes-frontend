export const state = () => ({
  form: null
})

export const getters = {
  getForm: (state) => state.form
}

export const mutations = {
  updateForm (state, payload) {
    state.form = payload
  }
}

export const actions = {
  async updateForm(store, payload) {
    store.commit('updateForm', payload)
  }
}
