# yes-frontend

## Live Demo
http://167.172.28.192:8080/

## Features to test
- Sign up an account
- Sign in with created account. You can also sign in with pre-generated account (username: yesdoctor, password: yesdoctor)
- Submit claim. Claim amount will be added to your credit balance
- Scan and pay. Paid amount will be deducted from your credit balance


## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
