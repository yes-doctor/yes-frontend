/* eslint-disable no-console */
import axios from 'axios'
import CONFIG from '../config'

const utils = {}

utils.loadAccount = () => {
  try {
    const account = localStorage.getItem('account')
    return JSON.parse(account)
  } catch {
    return null
  }
}
utils.getProfile = async (userId, token) => {
  try {
    const url = `${CONFIG.serverUrl}/api/profile/${userId}`
    const authOption = {
      method: 'GET',
      url,
      headers: {
        'Content-Type': 'application/json',
        'token': token
      }
    }
    const response = await axios(authOption)
    return response.data.profile
  } catch {
    return null
  }
}
utils.submitClaim = async (token, claim) => {
  try {
    const url = `${CONFIG.serverUrl}/api/claim`
    const authOption = {
      method: 'POST',
      url,
      data: claim,
      headers: {
        'Content-Type': 'application/json',
        token: token
      }
    }
    const response = await axios(authOption)
    console.log(response.data)
    return response.data
  } catch {
    return null
  }
}

export default utils
